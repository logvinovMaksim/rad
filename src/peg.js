/* eslint react/prop-types: "off" */

// outsource dependencies
import React from 'react';

// local dependencies

// configuration
export default Peg;

// TODO developer component to test router features
function Peg (props) {

    return (
        <div>
            <div className="row">
                <h3 className="col-xs-10 col-xs-offset-1 text-center top-indent-4">
                    Peg page =)
                </h3>
            </div>
            <div className="row">
                <h3 className="col-xs-10 col-xs-offset-1 text-center top-indent-4">
                    Props
                    <hr/>
                    <code> { JSON.stringify(props) } </code>
                    <hr/>
                </h3>
            </div>
        </div>
    );
}

