
// outsource dependencies
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import React, { PureComponent } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

// local dependencies
import Layout from './layout';
import { config } from '../constants';
import * as ROUTES from '../constants/routes';
import { MENU_ITEM_TYPE } from '../constants/spec';

// nested pages
import NoMatch from '../no-match';
import WelcomePage from './welcome';

import Users from './users';
// eslint-disable-next-line no-duplicate-imports
import ReportsPage from '../peg';
// eslint-disable-next-line no-duplicate-imports
import SettingsPage from '../peg';
// eslint-disable-next-line no-duplicate-imports
import HomeCtrlPage from '../peg';
// eslint-disable-next-line no-duplicate-imports
import PermissionsPage from '../peg';

class PrivateLayout extends PureComponent {
    render () {
        const { location, auth } = this.props;
        // NOTE redirect for unauthorized users
        if (!auth) { return (<Redirect to={{ pathname: ROUTES.SIGN_IN.ROUTE, state: { from: location } }}/>); }
        // NOTE authorized users routes
        return (<Layout menu={MENU}>
            <Switch>
                <Route path={ROUTES.PRIVATE_WELCOME_SCREEN.ROUTE} component={ WelcomePage } />
                <Route path={ROUTES.HOME_CTR.ROUTE} component={ HomeCtrlPage } />
                <Route path={ROUTES.USERS.ROUTE} component={ Users } />
                <Route path={ROUTES.REPORTS.ROUTE} component={ ReportsPage } />
                <Route path={ROUTES.PERMISSIONS.ROUTE} component={ PermissionsPage } />
                <Route path={ROUTES.SETTINGS.ROUTE} component={ SettingsPage } />
                {/* OTHERWISE */}
                { !config.production ? (
                    <Route component={ NoMatch } />
                ) : (// TODO on otherwise should redirect
                    <Redirect to={{ pathname: ROUTES.PRIVATE_WELCOME_SCREEN.LINK(), state: { from: location } }}/>
                )}
            </Switch>
        </Layout>);
    }
}
// Check
PrivateLayout.propTypes = {
    auth: PropTypes.bool.isRequired,
    location: PropTypes.object.isRequired,
};
// Export
export default connect(
    state => ({ auth: state.app.auth }),
    null
)(PrivateLayout);

// Side Menu
const MENU = [
    {
        disabled: true,
        type: MENU_ITEM_TYPE.LINK,
        name: 'Home Page controller',
        icon: 'fa fa-home',
        link: ROUTES.HOME_CTR.LINK(),
        // NOTE ability to custom detection of active state
        isActive: (s, location) => ROUTES.HOME_CTR.REGEXP.test(location.pathname)
    }, {
        type: MENU_ITEM_TYPE.LINK,
        name: 'User List',
        icon: 'fa fa-users',
        link: ROUTES.USERS.LINK(),
        isActive: (s, location) => ROUTES.USERS.REGEXP.test(location.pathname)
    }, {
        type: MENU_ITEM_TYPE.MENU,
        name: 'Settings menu',
        list: [
            {
                type: MENU_ITEM_TYPE.LINK,
                name: 'Permissions',
                icon: 'fa fa-handshake-o',
                link: ROUTES.PERMISSIONS.LINK(),
            }, {
                type: MENU_ITEM_TYPE.LINK,
                name: 'Reports',
                icon: 'fa fa-line-chart',
                link: ROUTES.REPORTS.LINK(),
            }, {
                type: MENU_ITEM_TYPE.LINK,
                name: 'Settings',
                icon: 'fa fa-cogs',
                link: ROUTES.SETTINGS.LINK(),
            }
        ]
    }
];
